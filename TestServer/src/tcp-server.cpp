//------------------------------------------------------------------------------
//
//      Основной класс, реализующий функционал сервера
//      (с) РГУПС, ВЖД 07/01/2017
//      Разработал: Притыкин Д.Е.
//
//------------------------------------------------------------------------------
#include    "tcp-server.h"

#include    <QDebug>

//------------------------------------------------------------------------------
//
//------------------------------------------------------------------------------
TcpServer::TcpServer()
{
    server_status = SERVER_STOPED;
    server = nullptr;
    port = 1992;

    server = new QTcpServer();
}

//------------------------------------------------------------------------------
//
//------------------------------------------------------------------------------
TcpServer::~TcpServer()
{

}

//------------------------------------------------------------------------------
//
//------------------------------------------------------------------------------
bool TcpServer::start()
{
    bool exit_code = false;

    if (server == nullptr)
        return false;

    connect(server, SIGNAL(newConnection()), this, SLOT(clientConnection()));

    if ( (!server->listen(QHostAddress::Any, port)) &&
         (server_status == SERVER_STOPED) )
    {
        server_status = SERVER_STOPED;
        qDebug() << "FAIL: Server is not started";
        exit_code = false;
    }
    else
    {
        server_status = SERVER_STARTED;
        qDebug() << "Server is started";
        exit_code = true;
    }

    return exit_code;
}

//------------------------------------------------------------------------------
//
//------------------------------------------------------------------------------
void TcpServer::clientConnection()
{
    if (server_status == SERVER_STARTED)
    {
        client_t *client = new client_t();

        client->socket = server->nextPendingConnection();
        int client_id = client->socket->socketDescriptor();

        clients.insert(client_id, client);

        connect(clients[client_id]->socket,
                SIGNAL(readyRead()),
                this,
                SLOT(receive()));

        connect(clients[client_id]->socket,
                SIGNAL(disconnected()),
                this,
                SLOT(clientDisconnected()));

        qDebug() << "Connected cliend: id = " << client_id;
    }
}

//------------------------------------------------------------------------------
//
//------------------------------------------------------------------------------
void TcpServer::receive()
{
    QTcpSocket *socket = (QTcpSocket *) sender();

    QByteArray buff = socket->readAll();

    tcp_video_recv_t recv_data;

    memcpy((unsigned char *) &recv_data, buff.data(), sizeof(recv_data));

    qDebug() << "Connected client name: " << recv_data.client_name;

    QMap<int, client_t *>::iterator it;

    for (it = clients.begin(); it != clients.end(); )
    {
        if (it.value()->socket == socket)
        {
            clients[it.key()]->name = recv_data.client_name;
            clients[it.key()]->is_connected = true;

            break;
        }
        else
        {
            ++it;
        }
    }
}

//------------------------------------------------------------------------------
//
//------------------------------------------------------------------------------
void TcpServer::clientDisconnected()
{
    QTcpSocket *socket = (QTcpSocket *) sender();

    QMap<int, client_t *>::iterator it;

    for (it = clients.begin(); it != clients.end(); )
    {
        if (it.value()->socket == socket)
        {
            qDebug() << "Disconnected client: "
                     << clients[it.key()]->name.toStdString().c_str();

            clients.remove(it.key());
            break;
        }
        else
        {
            ++it;
        }
    }
}

//------------------------------------------------------------------------------
//
//------------------------------------------------------------------------------
void TcpServer::resendData(tcp_video_t vd)
{
    // Отправка данных видеосистеме
    if ( (clients.count() != 0) && (server_status == SERVER_STARTED) )
    {
        QByteArray array = QByteArray::fromRawData((const char*) &vd,
                                                   sizeof(vd));

        QMap<int, client_t *>::iterator it;

        for (it = clients.begin(); it != clients.end(); )
        {
            if (clients[it.key()]->name == "VideoSystem")
            {
                clients[it.key()]->socket->write(array);
                clients[it.key()]->socket->flush();

                break;
            }
            else
            {
                ++it;
            }
        }
    }
}

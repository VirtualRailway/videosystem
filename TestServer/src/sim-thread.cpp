//------------------------------------------------------------------------------
//
//      Поток, имитирующий движение поезда
//      (с) РГУПС, 07/01/2017
//      Разработал: Притыкин Д.Е.
//
//------------------------------------------------------------------------------
#include    "sim-thread.h"

#include    <QTest>

//------------------------------------------------------------------------------
//
//------------------------------------------------------------------------------
SimThread::SimThread()
{
    delay = 100;
}

//------------------------------------------------------------------------------
//
//------------------------------------------------------------------------------
SimThread::~SimThread()
{

}

void SimThread::simulator()
{
    vd.time = 0;
    vd.dtime = (double) delay / 1000.0;
    vd.vehicle_type = 4;
    strcpy(vd.railway_name, "test");
    vd.direction = 1;
    vd.velocity = 0;
    vd.railway_coord = 0;

    while (1)
    {
        emit sendDataToTCP(vd);

        QTest::qSleep(delay);

        vd.velocity = 0;
        vd.railway_coord += (vd.velocity / 3.6) * vd.dtime;

        if (vd.velocity > 200)
            vd.velocity = 200;

        vd.time += vd.dtime;
        vd.count++;
    }
}

//------------------------------------------------------------------------------
//
//      Тестовый сервер для отладки видеосистемы
//      (с) РГУПС, ВЖД 07/01/2016
//      Разработал: Притыкин Д.Е.
//
//------------------------------------------------------------------------------
#include    "main.h"

//------------------------------------------------------------------------------
//
//------------------------------------------------------------------------------
int main(int argc, char *argv[])
{
    QCoreApplication a(argc, argv);

    TcpServer *server = new TcpServer();

    if (!server->start())
        return -1;

    qRegisterMetaType<tcp_video_t>();

    SimThread *simThread = new SimThread();

    QObject::connect(simThread, SIGNAL(sendDataToTCP(tcp_video_t)),
                     server, SLOT(resendData(tcp_video_t)));

    QThread *thread = new QThread();

    simThread->moveToThread(thread);

    QObject::connect(thread, SIGNAL(started()),
                     simThread, SLOT(simulator()));

    thread->start();

    return a.exec();
}

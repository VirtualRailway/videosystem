QT += core
QT -= gui
QT += network

CONFIG += c++11

TARGET = TestServer
CONFIG += console
CONFIG -= app_bundle
CONFIG += qtestlib

TEMPLATE = app

INCLUDEPATH += include/

CONFIG(debug, debug | release) {

    TARGET = $$join(TARGET,,,_d)
    DESTDIR = ../bin

} else {

    DESTDIR = ../bin
}

HEADERS += $$files(include/*.h)

SOURCES += $$files(src/*.cpp)

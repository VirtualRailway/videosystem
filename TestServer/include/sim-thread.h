//------------------------------------------------------------------------------
//
//      Поток, имитирующий движение поезда
//      (с) РГУПС, 07/01/2017
//      Разработал: Притыкин Д.Е.
//
//------------------------------------------------------------------------------
#ifndef SIMTHREAD_H
#define SIMTHREAD_H

#include    <QObject>
#include    "tcp-server.h"

//------------------------------------------------------------------------------
//
//------------------------------------------------------------------------------
class SimThread : public QObject
{
    Q_OBJECT

public:

    SimThread();
    ~SimThread();

public slots:

    void simulator();

signals:

    void sendDataToTCP(tcp_video_t vd);

private:

    tcp_video_t vd;

    int delay;
};

#endif // SIMTHREAD_H

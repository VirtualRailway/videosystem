//------------------------------------------------------------------------------
//
//      Основной класс, реализующий функционал сервера
//      (с) РГУПС, ВЖД 07/01/2017
//      Разработал: Притыкин Д.Е.
//
//------------------------------------------------------------------------------
#ifndef TCPSERVER_H
#define TCPSERVER_H

#include    <QObject>
#include    <QTcpServer>
#include    <QTcpSocket>
#include    <QMap>

#define     SERVER_STARTED  0
#define     SERVER_STOPED   1

//------------------------------------------------------------------------------
//      Структура данных, передаваемых видеосистеме
//------------------------------------------------------------------------------
#pragma pack(push, 1)

struct tcp_video_t
{
    unsigned char   vehicle_type;       // Тип подвижного состава
    char            railway_name[256];  // Имя участка ж/д
    double          time;               // Текущее модельное время
    double          dtime;              // Шаг времени между посылкой данных
    double          railway_coord;      // Железнодорожная координата, км
    double          velocity;           // Текущая скорость, км/ч
    signed char     direction;          // Направление движения
    uint64_t        count;              // Счетчик посылок

    tcp_video_t()
    {
        vehicle_type = 4;
        time = 0;
        dtime = 0;
        railway_coord = 0;
        velocity = 0;
        direction = 1;
        count = 0;
    }
};

#pragma pack(pop)

Q_DECLARE_METATYPE(tcp_video_t)

//------------------------------------------------------------------------------
//      Структура данных, принимаемая от видеосистемы
//------------------------------------------------------------------------------
#pragma pack(push, 1)
struct tcp_video_recv_t
{
    char client_name[256];     // Имя клиента
};
#pragma pack(pop)

//------------------------------------------------------------------------------
//      Структура описания подключенного клиента
//------------------------------------------------------------------------------
struct client_t
{
    QString     name;           // Имя клиента
    QTcpSocket  *socket;        // Сокет, окрытый клиентов для обмена
    bool        is_connected;   // Статус подключения

    client_t()
    {
        name = "";
        socket = nullptr;
        is_connected = false;
    }
};

//------------------------------------------------------------------------------
//
//------------------------------------------------------------------------------
class TcpServer : public QObject
{
    Q_OBJECT

public:

    TcpServer();
    ~TcpServer();

    bool start();

public slots:

    void clientConnection();

    void receive();

    void clientDisconnected();

    void resendData(tcp_video_t vd);

signals:

private:

    // состояние сервера
    int server_status;
    // объект для работы с сервером
    QTcpServer  *server;
    // список подключенных клиентов
    QMap<int, client_t *>   clients;

    int video_id;

    quint16 port;
};

#endif // TCPSERVER_H

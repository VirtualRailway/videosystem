//------------------------------------------------------------------------------
//
//      Базовый класс окна Qt-приложения с рендером Ogre
//      (c) РГУПС, ВЖД 05/01/2017
//      Разработал: Притыкин Д.Е.
//
//------------------------------------------------------------------------------
#ifndef QTOGREBASE_H
#define QTOGREBASE_H

#include    <iostream>

#include    <QtWidgets/QApplication>
#include    <QtGui/QKeyEvent>
#include    <QtGui/QWindow>

#include    <Ogre.h>
#include    <OgreOverlaySystem.h>

#include    "QtCameraMan.h"

#if defined QTOGREWINDOW
#define QTOGREWINDOW_COMMON_DLLSPEC Q_DECL_EXPORT
#else
#define QTOGREWINDOW_COMMON_DLLSPEC Q_DECL_IMPORT
#endif

// Размеры окна по-умолчанию
#define     DEFAULT_WIDTH   1024
#define     DEFAULT_HEIGHT  768

using namespace std;

//------------------------------------------------------------------------------
//
//------------------------------------------------------------------------------
class QTOGREWINDOW_COMMON_DLLSPEC QtOgreBaseWindow : public QWindow, 
                                                     public Ogre::FrameListener
{
    Q_OBJECT

public:

    // Конструктор класса
    explicit QtOgreBaseWindow(QWindow *parent = NULL,
                              int width = DEFAULT_WIDTH,
                              int height = DEFAULT_HEIGHT);

    // Деструктор класса
    virtual ~QtOgreBaseWindow();

    // Переопределяем метод отображения окна
    virtual void show(bool fullscreen = false);

    //
    void setAnimating(bool animating);

public slots:

    // Фильт событий Qt
    virtual bool eventFilter(QObject *watched, QEvent *event);

    //---- Слоты, связанные с работой Ogre ----
    virtual void renderLater();
    virtual void renderNow();

protected:

    //---- Переопределенные методы QWindow ----

    // Обработчик факта нажатия клавиши
    virtual void keyPressEvent(QKeyEvent *ev);
    // Обработчик факта отпускания клавиши
    virtual void keyReleaseEvent(QKeyEvent *ev);
    // Обработчик перемещения мыши
    virtual void mouseMoveEvent(QMouseEvent *e);
    // Обработчик колеса мыши
    virtual void wheelEvent(QWheelEvent *e);
    // Обработчик нажатия кнопки мыши
    virtual void mousePressEvent(QMouseEvent *e);
    // Обработчик отпускания кнопки мыши
    virtual void mouseReleaseEvent(QMouseEvent *e);

    // Обработчик появления окна после создания (повторного создания)
    virtual void exposeEvent(QExposeEvent *event);
    // Обработчик событий окна
    virtual bool event(QEvent *event);

    //---- Свойста, связанные с применением Ogre ----
    Ogre::RenderWindow          *mWindow;
    Ogre::Root                  *mRoot;
    bool                        m_update_pending;
    bool                        m_animating;

    Ogre::SceneManager          *mSceneMgr;
    Ogre::OverlaySystem         *mOverlaySystem;

    Ogre::Camera                *mCamera;

    OgreQtBites::QtCameraMan    *mCameraMan;

    //---- Методы, связанные с применением Ogre ----

    // инициализация
    virtual bool init();
    // настройка системы рендеринга и создание окна
    virtual bool configure();
    // настройка менеджера ресурсов
    virtual void setupResources();
    // создание и настройка менеджера сцены
    virtual void chooseSceneManager();
    // создание и предварительная настройка камеры
    virtual void createCamera();
    // настройка вьюпорта
    virtual void createViewPort();
    //
    virtual void createResourceListener();
    // загрузка ресурсов
    virtual void loadResources();
    // создание сцены, перезагрузить в наследнике
    virtual void createScene() = 0;
    //
    virtual void createFrameListener();
    //
    virtual bool frameRenderingQueued(const Ogre::FrameEvent &evt);

    virtual void render(QPainter *painter);
    virtual void render();

    void log(Ogre::String msg);
    void log(QString msg);
};

#endif // QTOGREBASE_H

//------------------------------------------------------------------------------
//
//      Базовый контроллер камеры для связки Qt + Ogre
//      (c) РГУПС, ВДЖ 06/01/2017
//      Разработал: Притыкин Д.Е.
//
//------------------------------------------------------------------------------
#ifndef QTCAMERAMAN_H
#define QTCAMERAMAN_H

#include    <OgreCamera.h>
#include    <OgreSceneNode.h>
#include    <OgreSceneManager.h>
#include    <OgreFrameListener.h>

#include    <QKeyEvent>
#include    <QMouseEvent>

#if defined QTCAMERAMAN
#define QTCAMERAMAN_COMMON_DLLSPEC Q_DECL_EXPORT
#else
#define QTCAMERAMAN_COMMON_DLLSPEC Q_DECL_IMPORT
#endif

//------------------------------------------------------------------------------
//
//------------------------------------------------------------------------------
namespace OgreQtBites
{
    enum CameraStyle
    {
        CS_FREELOOK,
        CS_ORBIT,
        CS_MANUAL
    };

    class QTCAMERAMAN_COMMON_DLLSPEC QtCameraMan
    {
    public:

        QtCameraMan(Ogre::Camera *cam);
        virtual ~QtCameraMan();

        // Установка и получение указателя на камеру
        virtual void setCamera(Ogre::Camera *cam);
        virtual Ogre::Camera *getCamera();

        // Установка цели, которую сопровождает камера
        virtual void setTarget(Ogre::SceneNode *target);
        virtual Ogre::SceneNode *getTarget();

        // Установить рыскание, тангаж камеры и дистанцию до цели
        virtual void setYawPitchDist(Ogre::Radian yaw,
                                     Ogre::Radian pitch,
                                     Ogre::Real dist);

        // Установить максимальную скорость камеры
        virtual void setTopSpeed(Ogre::Real topSpeed);
        virtual Ogre::Real getTopSpeed();

        // Установить стиль камеры
        virtual void setStyle(CameraStyle style);
        virtual CameraStyle getStyle();

        // Остановить камеру
        virtual void manualStop();

        // Обработчик перемещения камеры
        virtual bool frameRenderingQueued(const Ogre::FrameEvent &evt);

        // Обработчики средств ввода
        virtual void injectKeyDown(const QKeyEvent &evt);
        virtual void injectKeyUp(const QKeyEvent &evt);
        virtual void injectMouseMove(int relX, int relY);
        virtual void injectWheelMove(const QWheelEvent &evt);
        virtual void injectMouseDown(const QMouseEvent &evt);
        virtual void injectMouseUp(const QMouseEvent &evt);

    protected:

        Ogre::Camera        *mCamera;
        CameraStyle         mStyle;
        Ogre::SceneNode     *mTarget;

        bool                mOrbiting;
        bool                mZooming;

        Ogre::Real          mTopSpeed;
        Ogre::Vector3       mVelocity;

        // Флаги направления движения камеры
        bool                mForward;
        bool                mBack;
        bool                mLeft;
        bool                mRight;
        bool                mUp;
        bool                mDown;

        // Флаг ускоренного перемещения
        bool                mFastMove;
    };
}

#endif // QTCAMERAMAN_H

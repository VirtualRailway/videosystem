//------------------------------------------------------------------------------
//
//      Базовый класс окна Qt-приложения с рендером Ogre
//      (c) РГУПС, ВЖД 05/01/2017
//      Разработал: Притыкин Д.Е.
//
//------------------------------------------------------------------------------
#include    "QtOgreWindow.h"

const   string  CFG_PATH = "../cfg/";
const   string  LOG_PATH = "../logs/";

//------------------------------------------------------------------------------
//
//------------------------------------------------------------------------------
QtOgreBaseWindow::QtOgreBaseWindow(QWindow *parent,
                       int width,
                       int height) : QWindow(parent),
    mWindow(0),
    mRoot(0),
    m_update_pending(false),
    m_animating(false),
    mSceneMgr(0),
    mOverlaySystem(0),
    mCamera(0),
    mCameraMan(0)

{
    setAnimating(true);
    installEventFilter(this);

    this->setWidth(width);
    this->setHeight(height);
}

//------------------------------------------------------------------------------
//
//------------------------------------------------------------------------------
QtOgreBaseWindow::~QtOgreBaseWindow()
{
    if (mOverlaySystem)
        delete mOverlaySystem;

    delete mRoot;
}

//------------------------------------------------------------------------------
//
//------------------------------------------------------------------------------
bool QtOgreBaseWindow::eventFilter(QObject *watched, QEvent *event)
{
    if (watched == this)
    {
        if (event->type() == QEvent::Resize)
        {
            if ( this->isExposed() && (mWindow != NULL) )
            {
                mWindow->resize(this->width(), this->height());
            }
        }
    }

    return false;
}

//------------------------------------------------------------------------------
//
//------------------------------------------------------------------------------
void QtOgreBaseWindow::renderLater()
{
    if(!m_update_pending)
    {
        m_update_pending = true;
        QApplication::postEvent(this, new QEvent(QEvent::UpdateRequest));
    }
}

//------------------------------------------------------------------------------
//
//------------------------------------------------------------------------------
void QtOgreBaseWindow::renderNow()
{
    // Если окно не отображается, просто выходим
    if (!this->isExposed())
        return;

    // Если корневой элемент сцены не создан, выполняем инициализацию
    if (mRoot == NULL)
    {
        init();
    }

    // Рендерим сцену
    render();

    if (m_animating)
        renderLater();
}

//------------------------------------------------------------------------------
//
//------------------------------------------------------------------------------
void QtOgreBaseWindow::keyPressEvent(QKeyEvent *ev)
{
    if (ev->key() == Qt::Key_Escape)
        QApplication::exit();
}

//------------------------------------------------------------------------------
//
//------------------------------------------------------------------------------
void QtOgreBaseWindow::keyReleaseEvent(QKeyEvent *ev)
{
    Q_UNUSED(ev);
}

//------------------------------------------------------------------------------
//
//------------------------------------------------------------------------------
void QtOgreBaseWindow::mouseMoveEvent(QMouseEvent *e)
{
    Q_UNUSED(e);
}

//------------------------------------------------------------------------------
//
//------------------------------------------------------------------------------
void QtOgreBaseWindow::wheelEvent(QWheelEvent *e)
{
    Q_UNUSED(e);
}

//------------------------------------------------------------------------------
//
//------------------------------------------------------------------------------
void QtOgreBaseWindow::mousePressEvent(QMouseEvent *e)
{
    Q_UNUSED(e);
}

//------------------------------------------------------------------------------
//
//------------------------------------------------------------------------------
void QtOgreBaseWindow::mouseReleaseEvent(QMouseEvent *e)
{
    Q_UNUSED(e);
}

//------------------------------------------------------------------------------
//
//------------------------------------------------------------------------------
void QtOgreBaseWindow::exposeEvent(QExposeEvent *event)
{
    Q_UNUSED(event);

    // Выполняем рендеринг, если окно уже отображается
    if (this->isExposed())
        renderNow();
}

//------------------------------------------------------------------------------
//
//------------------------------------------------------------------------------
bool QtOgreBaseWindow::event(QEvent *event)
{
    switch (event->type())
    {
    case QEvent::UpdateRequest:

        m_update_pending = false;
        renderNow();
        return true;

    default:

        return QWindow::event(event);
    }
}

//------------------------------------------------------------------------------
//
//------------------------------------------------------------------------------
bool QtOgreBaseWindow::init()
{
    // Задаем имена файлов конфигурации и логов
    Ogre::String mPluginsCfg((CFG_PATH + "plugins.cfg").c_str());
    Ogre::String mOgreCfg((CFG_PATH + "ogre.cfg").c_str());
    Ogre::String mOgreLog((LOG_PATH + "Ogre.log").c_str());

    // Создаем корневой элемент сцены
    mRoot = new Ogre::Root(mPluginsCfg, mOgreCfg, mOgreLog);

    // Устанавливаем местоположение ресурсов
    setupResources();

    // Наcтраиваем систему рендеринга и окно
    if (!configure())
    {
        // в случае неудачи генерируем исключение
        OGRE_EXCEPT(Ogre::Exception::ERR_INVALIDPARAMS,
                    "Abort render system configuration",
                    "QtOgreWindow::initialize");

        return false;
    }

    // Создаем менеджер сцены
    chooseSceneManager();

    // Создаем камеру
    createCamera();

    // Создаем вьюпорт
    createViewPort();

    // Устанавливам мипмэп уровень
    Ogre::TextureManager::getSingleton().setDefaultNumMipmaps(5);

    // Создаем слушатель ресурсов (???)
    createResourceListener();

    // Загружаем ресурсы
    loadResources();

    // Создаем сцену
    createScene();

    createFrameListener();

    return true;
}

//------------------------------------------------------------------------------
//
//------------------------------------------------------------------------------
bool QtOgreBaseWindow::configure()
{
    // Получаем список доступных систем рендеринга
    const Ogre::RenderSystemList &rsList = mRoot->getAvailableRenderers();

    // Буфер для сохранения найденного рендера
    Ogre::RenderSystem *rs = rsList[0];

    // Список вариантов системы рендеринга. Заполняем его с учетом
    // возможности работы как под виндой, так и под линуксом
    Ogre::StringVector renderOrder;

#if defined(Q_OS_WIN)

    // Для винды DirectX
    renderOrder.push_back("Direct3D9");
    renderOrder.push_back("Direct3D11");

#endif

    // Для линукс или винды - OpenGL
    renderOrder.push_back("OpenGL");
    renderOrder.push_back("OpenGL 3+");

    // Пытаемся найти нужный рендер
    Ogre::StringVector::iterator iter;

    for (iter = renderOrder.begin(); iter != renderOrder.end(); iter++)
    {
        Ogre::RenderSystemList::const_iterator it;

        for (it = rsList.begin(); it != rsList.end(); it++)
        {
            size_t tmp((*it)->getName().find(*iter));

            if (tmp != Ogre::String::npos)
            {
                rs = *it;
                break;
            }
        }

        if (rs != NULL)
            break;
    }

    if (rs == NULL)
    {
        if (!mRoot->restoreConfig())
        {
            if (!mRoot->showConfigDialog())
            {
                return false;
            }
        }
    }

    // Устанавливаем параметры окна
    QString dim = QString("%1 x %2").arg(this->width()).arg(this->height());

    // устанавливаем размер окна
    rs->setConfigOption("Video Mode", dim.toStdString());
    // сбрасываем флаг полноэкранного режима
    rs->setConfigOption("Full Screen", "No");
    // включаем вертикальную синхронизацию
    rs->setConfigOption("VSync", "Yes");

    // Устанавливаем систему рендеринга для Ogre
    mRoot->setRenderSystem(rs);
    mRoot->initialise(false);

    // Пытаемся получить идентификатор окна
    Ogre::NameValuePairList params;

    if (rs->getName().find("GL") <= rs->getName().size())
        params["currentGLContext"] = Ogre::String("false");

#if defined(Q_OS_WIN)

    params["externalWindowHandle"] =
            Ogre::StringConverter::toString((size_t) (this->winId()));

    params["parentWindowHandle"] =
            Ogre::StringConverter::toString((size_t) (this->winId()));

#else

    params["externalWindowHandle"] =
            Ogre::StringConverter::toString((unsigned long) (this->winId()));

    params["parentWindowHandle"] =
            Ogre::StringConverter::toString((unsigned long) (this->winId()));

#endif

    mWindow = mRoot->createRenderWindow("QT Window",
                                        this->width(),
                                        this->height(),
                                        false,
                                        &params);

    mWindow->setVisible(true);

    return true;
}

//------------------------------------------------------------------------------
//
//------------------------------------------------------------------------------
void QtOgreBaseWindow::setupResources()
{
    // Имя файла конфигурации ресурсов
    Ogre::String mResourcesCfg((CFG_PATH + "resources.cfg").c_str());

    // Загружаем файл конфигурации
    Ogre::ConfigFile cf;
    cf.load(mResourcesCfg);

    // Получаем итератор секций файла
    Ogre::ConfigFile::SectionIterator seci = cf.getSectionIterator();

    Ogre::String secName;
    Ogre::String typeName;
    Ogre::String archName;

    // Пока не кончились секции в конфиге
    while (seci.hasMoreElements())
    {
        // Читаем имя секции
        secName = seci.peekNextKey();
        Ogre::ConfigFile::SettingsMultiMap *settings = seci.getNext();
        Ogre::ConfigFile::SettingsMultiMap::iterator i;

        // Читаем тип файловой системы ресурса и путь к нему
        for (i = settings->begin(); i != settings->end(); ++i)
        {
            typeName = i->first;    // Тип файловой системы
            archName = i->second;   // путь

            // Добавляем ресурсы в группу ресурсов
            Ogre::ResourceGroupManager::getSingleton().addResourceLocation(archName,
                                                                           typeName,
                                                                           secName);
        }
    }
}

//------------------------------------------------------------------------------
//
//------------------------------------------------------------------------------
void QtOgreBaseWindow::chooseSceneManager()
{
    mSceneMgr = mRoot->createSceneManager(Ogre::ST_GENERIC);

    mOverlaySystem = new Ogre::OverlaySystem();
    mSceneMgr->addRenderQueueListener(mOverlaySystem);
}

//------------------------------------------------------------------------------
//
//------------------------------------------------------------------------------
void QtOgreBaseWindow::createCamera()
{
    // Создаем камеру
    mCamera = mSceneMgr->createCamera("PlayerCam");
    // Устанавливаем её положение
    mCamera->setPosition(Ogre::Vector3(0, 0, 80));
    // Устанавливаем направление взгляда камеры
    mCamera->lookAt(Ogre::Vector3(0, 0, -300));
    mCamera->setNearClipDistance(5);
}

//------------------------------------------------------------------------------
//
//------------------------------------------------------------------------------
void QtOgreBaseWindow::createViewPort()
{
    Ogre::Viewport *vp = mWindow->addViewport(mCamera);
    vp->setBackgroundColour(Ogre::ColourValue(0, 0, 0));

    mCamera->setAspectRatio(Ogre::Real(mWindow->getWidth()) /
                            Ogre::Real(mWindow->getHeight()));

    mCamera->setAutoAspectRatio(true);
}

//------------------------------------------------------------------------------
//
//------------------------------------------------------------------------------
void QtOgreBaseWindow::createResourceListener()
{

}

//------------------------------------------------------------------------------
//
//------------------------------------------------------------------------------
void QtOgreBaseWindow::loadResources()
{
    Ogre::ResourceGroupManager::getSingleton().initialiseAllResourceGroups();
}

//------------------------------------------------------------------------------
//
//------------------------------------------------------------------------------
void QtOgreBaseWindow::createFrameListener()
{
    mRoot->addFrameListener(this);
}

//------------------------------------------------------------------------------
//
//------------------------------------------------------------------------------
bool QtOgreBaseWindow::frameRenderingQueued(const Ogre::FrameEvent &evt)
{
    Q_UNUSED(evt);

    return true;
}

//------------------------------------------------------------------------------
//
//------------------------------------------------------------------------------
void QtOgreBaseWindow::render(QPainter *painter)
{
    Q_UNUSED(painter);
}

//------------------------------------------------------------------------------
//
//------------------------------------------------------------------------------
void QtOgreBaseWindow::render()
{
    Ogre::WindowEventUtilities::messagePump();
    mRoot->renderOneFrame();
}

//------------------------------------------------------------------------------
//
//------------------------------------------------------------------------------
void QtOgreBaseWindow::log(Ogre::String msg)
{
    if (Ogre::LogManager::getSingletonPtr() != NULL)
        Ogre::LogManager::getSingletonPtr()->logMessage(msg);
}

//------------------------------------------------------------------------------
//
//------------------------------------------------------------------------------
void QtOgreBaseWindow::log(QString msg)
{
    log(Ogre::String(msg.toStdString().c_str()));
}

//------------------------------------------------------------------------------
//
//------------------------------------------------------------------------------
void QtOgreBaseWindow::show(bool fullscreen)
{
    // В зависимости от флага, выбираем режим отображения окна
    if (fullscreen)
        QWindow::showFullScreen();
    else
        QWindow::show();
}

//------------------------------------------------------------------------------
//
//------------------------------------------------------------------------------
void QtOgreBaseWindow::setAnimating(bool animating)
{
    m_animating = animating;

    if (animating)
        renderLater();
}


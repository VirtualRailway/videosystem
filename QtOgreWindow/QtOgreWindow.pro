#-------------------------------------------------------------------------------
#
#       Сценарий сборки для qmake
#
#
#-------------------------------------------------------------------------------

# Тип сборки и имя библиотеки
TEMPLATE = lib
TARGET = QtOgreWindow

# Включаемые модули Qt
QT += core gui
QT += widgets

# Конфигурация сборки для unix
unix {

    INCLUDEPATH += /usr/include/OGRE
    INCLUDEPATH += /usr/include/OGRE/Overlay    

    CONFIG += link_pkgconfig
    PKGCONFIG += OGRE
}

# Конфигурация сборки для винды
win32 {

}


CONFIG(debug, debug | release) {
    # Задаем суффикс для отладочного бинарника
    TARGET = $$join(TARGET,,,_d)
    # Отладочные версии библиотек
    LIBS *= -lOgreMain -lOIS -lOgreOverlay -lboost_system
    # Путь к исполняемому файлу
    DESTDIR = ../lib

} else {
    # Аналогичные настройки для релиза
    LIBS *= -lOgreMain -lOIS -lOgreOverlay -lboost_system
    DESTDIR = ../lib
}

# Путь поиска заголовков
INCLUDEPATH += include/

# Список заголовочных файлов
HEADERS += $$files(include/*.h)

# Исходники модулей
SOURCES += $$files(src/*.cpp)

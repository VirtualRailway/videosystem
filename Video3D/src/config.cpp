//------------------------------------------------------------------------------
//
//      Функции для чтения конфигурации видеосистемы
//      (с) РГУПС, 09/01/2017
//      Разработал: Притыкин Д.Е.
//
//------------------------------------------------------------------------------
#include    "config.h"

const   QString     LOCALHOST = "127.0.0.1";

//------------------------------------------------------------------------------
//
//------------------------------------------------------------------------------
void getWindowConfig(CfgReader cfg, window_config_t &window_config)
{
    TMsg    msg;

    if (!cfg.getInt("Window", "Width", window_config.width, msg))
    {
        window_config.width = DEFAULT_WIDTH;
    }

    if (!cfg.getInt("Window", "Height", window_config.height, msg))
    {
        window_config.height = DEFAULT_HEIGHT;
    }

    int tmp = 0;

    if (!cfg.getInt("Window", "FullScreen", tmp, msg))
    {
        tmp = 0;
    }

    window_config.fullscreen = (bool) tmp;
}

//------------------------------------------------------------------------------
//
//------------------------------------------------------------------------------
void getTcpClientConfig(CfgReader cfg, tcp_config_t &tcp_config)
{
    TMsg msg;

    if (!cfg.getString("TCP", "Name", tcp_config.name, msg))
    {
        tcp_config.name = "VideoSystem";
    }

    if (!cfg.getString("TCP", "HostAddr", tcp_config.host_addr, msg))
    {
        tcp_config.host_addr = LOCALHOST;
    }

    int tmp = 0;

    if (!cfg.getInt("TCP", "Port", tmp, msg))
    {
        tmp = 1992;
    }

    tcp_config.port = (quint16) tmp;

    if (!cfg.getInt("TCP", "Timeout", tcp_config.ms_timeout, msg))
    {
        tcp_config.ms_timeout = DEFAULT_TIMEOUT;
    }
}

//-----------------------------------------------------------------------------
//
//		Функции для перевода строк в числа
//		(с) РГУПС, ВЖД 17/09/2016
//		Разработал: Притыкин Д.Е.
//
//-----------------------------------------------------------------------------
#include	"convert.h"

//-----------------------------------------------------------------------------
//
//-----------------------------------------------------------------------------
bool TextToDouble(QString text, double &value, TMsg &msg)
{
	bool validate = false;	// Флаг проверки корректности данных

	// Пытаемся выполнить конвертацию данных
	value = text.toDouble(&validate);
	
	// Если данные не вещественные - выход с предупреждением
	if (!validate)
	{
		msg.type = MSG_WARN;
		msg.code = WARN_NOT_DOUBLE_DATA;
		msg.description = "Input data " + text + " is not double";		
		return false;
	}

	return true;
}

//-----------------------------------------------------------------------------
//
//-----------------------------------------------------------------------------
bool TextToInt(QString text, int &value, TMsg &msg)
{
	bool validate = false;
	value = text.toInt(&validate);
	
	if (!validate)
	{
		msg.type = MSG_WARN;
		msg.code = WARN_NOT_INTEGER_DATA;
		msg.description = "Input data " + text + " is not integer";		
		return false;
	}

	return true;
}

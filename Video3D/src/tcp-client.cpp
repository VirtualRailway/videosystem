//------------------------------------------------------------------------------
//
//      TCP-клиент, принимающий данные от сервера
//      (с) РГУПС, ВЖД 06/01/2017
//      Разработал: Притыкин Д.Е.
//
//------------------------------------------------------------------------------
#include    "tcp-client.h"

//------------------------------------------------------------------------------
//
//------------------------------------------------------------------------------
TcpClient::TcpClient()
{
    socket = nullptr;
    tcp_state.is_connected = false;
    tcp_state.count = 0;
}

//------------------------------------------------------------------------------
//
//------------------------------------------------------------------------------
TcpClient::~TcpClient()
{

}

//------------------------------------------------------------------------------
//
//------------------------------------------------------------------------------
void TcpClient::init(tcp_config_t tcp_config)
{
    // Сохраняем данные конфигурации
    this->tcp_config = tcp_config;

    // Создаем клиентский сокет
    socket = new QTcpSocket();
    in.setDevice(socket);
    in.setVersion(QDataStream::Qt_4_0);

    // Связываем сигналы и слоты

    // прием данных
    connect(socket, SIGNAL(readyRead()), this, SLOT(receive()));

    // обработка факта соединения
    connect(socket, SIGNAL(connected()), this, SLOT(onConnect()));

    // обработка факта дисконнекта
    connect(socket, SIGNAL(disconnected()), this, SLOT(onDisconnect()));
}

//------------------------------------------------------------------------------
//
//------------------------------------------------------------------------------
void TcpClient::connection()
{
    // Сбрасываем сокет
    socket->abort();
    tcp_state.is_connected = false;
    tcp_state.count = 0;

    // Выполняем соединение
    socket->connectToHost(tcp_config.host_addr,
                          tcp_config.port,
                          QIODevice::ReadWrite);
}

//------------------------------------------------------------------------------
//
//------------------------------------------------------------------------------
void TcpClient::send(tcp_output_t tcp_output)
{
    // Если сокет открыт
    if (socket->isOpen())
    {
        // Преобразуем данные к формату, пригодному для передачи
        QByteArray array = QByteArray::fromRawData((const char *) &tcp_output,
                                                   sizeof(tcp_output));

        // Передаем данные
        socket->write(array);
        socket->flush();
    }
}

//------------------------------------------------------------------------------
//
//------------------------------------------------------------------------------
bool TcpClient::isConnected()
{
    return tcp_state.is_connected;
}

//------------------------------------------------------------------------------
//
//------------------------------------------------------------------------------
void TcpClient::receive()
{
    // Принимаем данные в буфер
    QByteArray buff = socket->readAll();

    // Подготавливаем структуру для приема данных
    main_window_data_t main_window_data;

    // Копируем данные
    memcpy((unsigned char *) &main_window_data.tcp_input, buff.data(), sizeof(tcp_input_t));

    tcp_state.count++;
    main_window_data.count = tcp_state.count;

    // Отправляем данные окну
    emit sendDataToWindow(main_window_data);
}

//------------------------------------------------------------------------------
//
//------------------------------------------------------------------------------
void TcpClient::onConnect()
{
    // Взводим флаг соединения
    tcp_state.is_connected = true;
    tcp_state.count = 0;

    // Отправляем серверу сигнатуру - имя клиента
    tcp_output_t tcp_output;

    strcpy(tcp_output.client_name, tcp_config.name.toStdString().c_str());

    send(tcp_output);
}

//------------------------------------------------------------------------------
//
//------------------------------------------------------------------------------
void TcpClient::onDisconnect()
{
    tcp_state.is_connected = false;
    tcp_state.count = 0;
}

//------------------------------------------------------------------------------
//
//------------------------------------------------------------------------------
void TcpClient::sendTcpState(tcp_state_t *tcp_state)
{
    *tcp_state = this->tcp_state;
}

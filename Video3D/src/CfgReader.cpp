//-----------------------------------------------------------------------------
//
//		Класс для чтения конфигурационных XML-файлов
//		(с) РГУПС, ВЖД 17/09/2016
//		Разработал: Притыкин Д.Е.
//
//-----------------------------------------------------------------------------
#include "CfgReader.h"
#include "convert.h"

//-----------------------------------------------------------------------------
//
//-----------------------------------------------------------------------------
CfgReader::CfgReader()
{

}

//-----------------------------------------------------------------------------
//
//-----------------------------------------------------------------------------
CfgReader::~CfgReader()
{
	
}

//-----------------------------------------------------------------------------
//
//-----------------------------------------------------------------------------
bool CfgReader::load(QString path, TMsg &msg)
{
	// Пытаемся открыть конфигурационный файл
	file_name = path;
	file = new QFile(file_name);

	if (!file->open(QFile::ReadOnly | QFile::Text))
	{
		// Если это не удалось, выводим предупреждение об его отсутствии
		msg.type = MSG_WARN;
		msg.code = WARN_CFG_FILE_NOT_FOUND;
		msg.description = "File " + file_name + " is not found";

		return false;
	}

	// Читаем содержимое файла
	domDoc.setContent(file);

	// Закрываем файл
	file->close();

	// Читаем корневой элемент
	firstElement = domDoc.documentElement();

	// Проверяем имя корневого элемента
	if (firstElement.tagName() != "Config")
	{
		msg.type = MSG_WARN;
		msg.code = WARN_XML_INVALID_ROOT;
		msg.description = "Invalid root element in " + path;
		return false;
	}	
	
	return true;
}

//-----------------------------------------------------------------------------
//
//-----------------------------------------------------------------------------
bool CfgReader::getString(QString section,
						  QString field,
						  QString &value,
						  TMsg &msg)
{
	QDomNode secNode = getFirstSection(section);

	if (secNode.isNull())
	{
		msg.type = MSG_WARN;
		msg.code = WARN_XML_SECTION_NOT_FOUND;
		msg.description = "Section " + section + " is not found in " + file_name;
		return false;
	}

	QDomNode fieldNode = getField(secNode, field);

	if (fieldNode.isNull())
	{
		msg.type = MSG_WARN;
		msg.code = WARN_XML_FIELD_NOT_FOUND;
		msg.description = "Field " + field + " is not found in section " + section + " of " + file_name;
		return false;
	}

	value = fieldNode.toElement().text();

	return true;
}

//-----------------------------------------------------------------------------
//
//-----------------------------------------------------------------------------
bool CfgReader::getDouble(QString section, QString field, double &value, TMsg &msg)
{
	QString tmp;
	if (!getString(section, field, tmp, msg))
	{
		return false;
	}

	if (!TextToDouble(tmp, value, msg))
	{
		return false;
	}

	return true;
}

//-----------------------------------------------------------------------------
//
//-----------------------------------------------------------------------------
bool CfgReader::getInt(QString section, QString field, int &value, TMsg &msg)
{
	QString tmp;
	if (!getString(section, field, tmp, msg))
	{
		return false;
	}

	if (!TextToInt(tmp, value, msg))
	{
		return false;
	}

	return true;
}

//-----------------------------------------------------------------------------
//
//-----------------------------------------------------------------------------
QDomNode CfgReader::getFirstSection(QString section)
{
	QDomNode node = firstElement.firstChild();

	while ( (node.nodeName() != section) && (!node.isNull()) )
	{
		node = node.nextSibling();
	}

	curNode = node;

	return node;
}

//-----------------------------------------------------------------------------
//
//-----------------------------------------------------------------------------
QDomNode CfgReader::getNextSection()
{
	QString section = curNode.nodeName();
	QDomNode node = curNode.nextSibling();

	while ((node.nodeName() != section) && (!node.isNull()))
	{
		node = node.nextSibling();
	}

	curNode = node;

	return node;
}

//-----------------------------------------------------------------------------
//
//-----------------------------------------------------------------------------
QDomNode CfgReader::getField(QDomNode secNode, QString field)
{
	QDomNode node = secNode.firstChild();

	while ((node.nodeName() != field) && (!node.isNull()))
	{
		node = node.nextSibling();
	}

	return node;
}

//-----------------------------------------------------------------------------
//
//-----------------------------------------------------------------------------
bool CfgReader::getString(QDomNode secNode, QString field, QString &value, TMsg &msg)
{
	QDomNode node = getField(secNode, field);

	if (node.isNull())
	{
		msg.type = MSG_WARN;
		msg.code = WARN_XML_FIELD_NOT_FOUND;
		msg.description = "Field " + field + " is not found in section " + secNode.nodeName() + " of " + file_name;
		return false;
	}

	value = node.toElement().text();

	return true;
}

//-----------------------------------------------------------------------------
//
//-----------------------------------------------------------------------------
bool CfgReader::getDouble(QDomNode secNode, QString field, double &value, TMsg &msg)
{
	QString tmp;

	if (!getString(secNode, field, tmp, msg))
	{
		return false;
	}

	if (!TextToDouble(tmp, value, msg))
	{
		return false;
	}

	return true;
}

//-----------------------------------------------------------------------------
//
//-----------------------------------------------------------------------------
bool CfgReader::getInt(QDomNode secNode, QString field, int &value, TMsg &msg)
{
	QString tmp;

	if (!getString(secNode, field, tmp, msg))
	{
		return false;
	}

	if (!TextToInt(tmp, value, msg))
	{
		return false;
	}

	return true;
}

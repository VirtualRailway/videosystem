//------------------------------------------------------------------------------
//
//      Видеосистема тренажеров подвижного состава
//      (с) РГУПС, ВЖД 06/01/2017
//      Разработал: Притыкин Д.Е.
//
//------------------------------------------------------------------------------
#include    "main.h"

const   QString     CFG_PATH = "../cfg/";

//------------------------------------------------------------------------------
//
//------------------------------------------------------------------------------
int main(int argc, char *argv[])
{
    // Создаем приложение Qt, передаем ему параметры
    QApplication a(argc, argv);

    CfgReader   cfg;
    TMsg        msg;

    cfg.load(CFG_PATH + "config.xml", msg);

    // Читаем конфигурацию окна
    window_config_t window_config;

    getWindowConfig(cfg, window_config);

    // Читаем конфигурацию TCP-клиента
    tcp_config_t tcp_config;

    getTcpClientConfig(cfg, tcp_config);

    // Создаем основное окно
    MainWindow *mainWindow = new MainWindow(NULL,
                                            window_config.width,
                                            window_config.height);

    // Создаем TCP-клиент
    TcpClient *tcp_client = new TcpClient();

    // Регистрируем все типы данных, передаваемых сигналами
    qRegisterMetaType<main_window_data_t>();
    qRegisterMetaType<tcp_state_t>();

    // Связываем сигнал TCP-клиенты и слот окна для передачи данных
    QObject::connect(tcp_client,
                     SIGNAL(sendDataToWindow(main_window_data_t)),
                     mainWindow,
                     SLOT(recieveData(main_window_data_t)));

    // Инициализируем клиент
    tcp_client->init(tcp_config);

    // Создаем котролер TCP-соединения
    TcpConnector *tcp_connector = new TcpConnector(tcp_config.ms_timeout);

    // Связываем сисгналы слоты для контроля соединения

    // получение статуса клиента
    QObject::connect(tcp_connector,
                     SIGNAL(getTcpState(tcp_state_t*)),
                     tcp_client,
                     SLOT(sendTcpState(tcp_state_t*)));

    // установка соединения
    QObject::connect(tcp_connector,
                     SIGNAL(connection()),
                     tcp_client,
                     SLOT(connection()));

    // передача статуса главному окну
    QObject::connect(tcp_connector,
                     SIGNAL(sendTcpStateToWindow(tcp_state_t)),
                     mainWindow,
                     SLOT(recieveTcpStateData(tcp_state_t)));

    // Создаем поток контроллера
    QThread *tcp_thread = new QThread();

    // Помещаем контроллер в поток
    tcp_connector->moveToThread(tcp_thread);

    // Связываем сигнал запуска потока с функцией потока
    QObject::connect(tcp_thread,
                     SIGNAL(started()),
                     tcp_connector,
                     SLOT(process()));

    // Запускаем поток контроллера TCP
    tcp_thread->start();

    // Отображаем основное окно
    mainWindow->show(window_config.fullscreen);

    // Запускам цикл обработки сигналов
    int exit_code = a.exec();

    // Останавливаем поток контролера TCP
    tcp_thread->terminate();

    // Уничтожаем контролер
    delete tcp_connector;

    // Уничтожаем клиент
    delete tcp_client;
    // Уничтожаем окно
    delete mainWindow;

    return exit_code;
}

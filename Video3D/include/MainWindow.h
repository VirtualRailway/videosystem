//------------------------------------------------------------------------------
//
//      Главное окно видеосистемы
//      (с) РГУПС, ВЖД 06/01/2017
//      Разработал: Притыкин Д.Е.
//
//------------------------------------------------------------------------------
#ifndef     MAINWINDOW_H
#define     MAINWINDOW_H

#include    "QtOgreWindow.h"
#include    "common-types.h"
#include    "tcp-data.h"

//------------------------------------------------------------------------------
//
//------------------------------------------------------------------------------
class MainWindow : public QtOgreBaseWindow
{
    Q_OBJECT

public:

    explicit MainWindow(QWindow *parent = NULL,
                        int width = DEFAULT_WIDTH,
                        int height = DEFAULT_HEIGHT);

    virtual ~MainWindow();

public slots:

    // Прием данных от TCP-клиента
    void recieveData(main_window_data_t main_window_data);

    // Прием данных состоянии TCP-клиента
    void recieveTcpStateData(tcp_state_t tcp_state);

protected:

    // Метод, в котором формируется всё содержимое сцены
    virtual void createScene();

    // Данные, принятые от TCP-клиента
    main_window_data_t      main_window_data;
    // Предыдущий пакет данных
    tcp_input_t             tcp_input_prev;

    // Состояние TCP-клиента
    tcp_state_t             tcp_state;
};

#endif // MAINWINDOW_H

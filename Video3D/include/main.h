//------------------------------------------------------------------------------
//
//      Видеосистема тренажеров подвижного состава
//      (с) РГУПС, ВЖД 06/01/2017
//      Разработал: Притыкин Д.Е.
//
//------------------------------------------------------------------------------
#ifndef MAIN_H
#define MAIN_H

#include    <QObject>
#include    <QThread>

#include    "common-types.h"
#include    "config.h"
#include    "CfgReader.h"
#include    "MainWindow.h"
#include    "tcp-client.h"
#include    "tcp-connector.h"

#endif // MAIN_H

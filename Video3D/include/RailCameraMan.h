//------------------------------------------------------------------------------
//
//          Менеджер камеры, реализующий её движение по рельсам
//          (с) РГУПС, ВЖД 09/01/2017
//          Разработал: Притыкин Д.Е.
//
//------------------------------------------------------------------------------
#ifndef     RAILCAMERAMAN_H
#define     RAILCAMERAMAN_H

#include    <OgreCamera.h>
#include    <OgreSceneNode.h>
#include    <OgreSceneManager.h>
#include    <OgreFrameListener.h>

//------------------------------------------------------------------------------
//
//------------------------------------------------------------------------------
class RailCameraMan
{
public:

    RailCameraMan(Ogre::Camera *cam);
    virtual ~RailCameraMan();

protected:

    Ogre::Camera    *mCamera;
};

#endif  // RAILCAMERAMAN_H

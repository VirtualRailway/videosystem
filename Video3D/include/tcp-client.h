//------------------------------------------------------------------------------
//
//      TCP-клиент, принимающий данные от сервера
//      (с) РГУПС, ВЖД 06/01/2017
//      Разработал: Притыкин Д.Е.
//
//------------------------------------------------------------------------------
#ifndef TCPCLIENT_H
#define TCPCLIENT_H

#include    <QtGlobal>
#include    <QObject>
#include    <QTcpSocket>
#include    <QDataStream>
#include    <QMetaType>

#include    "common-types.h"
#include    "tcp-data.h"

//------------------------------------------------------------------------------
//
//------------------------------------------------------------------------------
class TcpClient : public QObject
{
    Q_OBJECT

public:

    TcpClient();
    ~TcpClient();

    // Инициализация клиента
    void init(tcp_config_t tcp_config);

    // Посылка данных серверу
    void send(tcp_output_t tcp_output);

    // Проверка соединения
    bool isConnected();

public slots:

    // Соединение с сервером
    void connection();

    // Прием данных от сервера
    void receive();

    // Факт соединения с сервером
    void onConnect();

    // Факт разрыва соединения с сервером
    void onDisconnect();

    // Послать состояние клиента другому классу
    void sendTcpState(tcp_state_t *tcp_state);

signals:

    // Передача данных главному окну видеосистемы
    void sendDataToWindow(main_window_data_t main_window_data);

private:

    // Данные конфигурации клиента
    tcp_config_t    tcp_config;

    // Клиентский сокет
    QTcpSocket      *socket;
    // Входящие поток данных
    QDataStream     in;

    // Статус TCP-клиента
    tcp_state_t     tcp_state;
};

#endif // TCPCLIENT_H

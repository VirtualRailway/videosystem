//-----------------------------------------------------------------------------
//
//		Функции для перевода строк в числа
//		(с) РГУПС, ВЖД 17/09/2016
//		Разработал: Притыкин Д.Е.
//
//-----------------------------------------------------------------------------
#ifndef		CONVERT_H
#define		CONVERT_H

#include	<QString>

#include	"msg.h"

// Перевод текста в вещественное число
bool TextToDouble(QString text, double &value, TMsg &msg);
// Перевод текста в целое число
bool TextToInt(QString text, int &value, TMsg &msg);

#endif

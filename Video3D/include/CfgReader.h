//-----------------------------------------------------------------------------
//
//		Класс для чтения конфигурационных XML-файлов
//		(с) РГУПС, ВЖД 17/09/2016
//		Разработал: Притыкин Д.Е.
//
//-----------------------------------------------------------------------------
#ifndef CFGREADER_H
#define CFGREADER_H

#include <QDomDocument>
#include <QDomElement>
#include <QDomNode>
#include <QString>
#include <QFile>

#include "msg.h"

//-----------------------------------------------------------------------------
//
//-----------------------------------------------------------------------------
class CfgReader
{
public:

	CfgReader();
	~CfgReader();

	// Загрузка конфигурационного файла
	bool load(QString path, TMsg &msg);		

	// Найти первую секцию с заданным именем
	QDomNode getFirstSection(QString section);
	// Найти следующую секцию с таким же именем
	QDomNode getNextSection();

	// Найти нужное поле в секции
	QDomNode getField(QDomNode secNode, QString field);

	// Прочесть поле конфига в текстовом формате
	bool getString(QString section, QString field, QString &value, TMsg &msg);
	bool getString(QDomNode secNode, QString field, QString &value, TMsg &msg);
	// Прочесть вещественное поле конфига
	bool getDouble(QString section, QString filed, double &value, TMsg &msg);
	bool getDouble(QDomNode secNode, QString field, double &value, TMsg &msg);
	// Прочесть целочисленное поле конфига
	bool getInt(QString section, QString filed, int &value, TMsg &msg);
	bool getInt(QDomNode secNode, QString field, int &value, TMsg &msg);

private:

	QFile *file;
	QString file_name;
	QDomDocument domDoc;
	QDomElement firstElement;
	QDomNode curNode;	
};

#endif // CFGREADER_H


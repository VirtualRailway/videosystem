//------------------------------------------------------------------------------
//
//      Структуры данных для обмена с сервером
//      (с) РГУПС, ВЖД 06/01/2017
//      Разработал: Притыкин Д.Е.
//
//------------------------------------------------------------------------------
#ifndef TCPDATA_H
#define TCPDATA_H

#include    <QMetaType>

//------------------------------------------------------------------------------
//      Принимаемые данные
//------------------------------------------------------------------------------
#pragma pack(push, 1)

struct tcp_input_t
{
    unsigned char   vehicle_type;       // Тип подвижного состава
    char            railway_name[256];  // Имя участка ж/д
    double          time;               // Текущее модельное время
    double          dtime;              // Шаг времени между посылкой данных
    double          railway_coord;      // Железнодорожная координата, км
    double          velocity;           // Текущая скорость, км/ч
    signed char     direction;          // Направление движения

    tcp_input_t()
    {
        vehicle_type = 4;
        strcpy(railway_name, "test");
        time = 0;
        dtime = 0;
        railway_coord = 0;
        velocity = 0;
        direction = 1;
    }
};

#pragma pack(pop)

Q_DECLARE_METATYPE(tcp_input_t)

//------------------------------------------------------------------------------
//      Передаваемые данные
//------------------------------------------------------------------------------
#pragma pack(push, 1)
struct tcp_output_t
{
    char    client_name[256];     // Имя клиента
};
#pragma pack(pop)

Q_DECLARE_METATYPE(tcp_output_t)

//------------------------------------------------------------------------------
//      Статус TCP-клиента
//------------------------------------------------------------------------------
struct tcp_state_t
{
    bool            is_connected;
    uint64_t        count;

    tcp_state_t()
    {
        is_connected = false;
        count = 0;
    }
};

Q_DECLARE_METATYPE(tcp_state_t)

#endif // TCPDATA_H

//------------------------------------------------------------------------------
//
//      Класс, управляющий соединением с сервером
//      (с) РГУПС, ВЖД 07/01/2017
//      Разработал: Притыкин Д.Е.
//
//------------------------------------------------------------------------------
#ifndef TCP_CONNECTOR_H
#define TCP_CONNECTOR_H

#include    <QObject>

#include    "tcp-data.h"

#define     DEFAULT_TIMEOUT     1000

//------------------------------------------------------------------------------
//
//------------------------------------------------------------------------------
class TcpConnector : public QObject
{
    Q_OBJECT

public:

    TcpConnector(int timeout = DEFAULT_TIMEOUT);
    ~TcpConnector();

public slots:

    // Метод, выполняемый в отдельном потоке, обсепечивающий
    // контроль за соединением
    void process();

signals:

    // Получить состояние клиента
    void getTcpState(tcp_state_t *tcp_state);

    // Подключится к серверу
    void connection();

    // Послать состояние клиента главному окну
    void sendTcpStateToWindow(tcp_state_t tcp_state);

private:

    // Текущее состояние клиента
    tcp_state_t     tcp_state;

    // Таймаут между попытками соединения
    int timeout;

    // Отсчет времени ожидания соедиенения с сервером
    int time;
};

#endif // TCP_CONNECTOR_H

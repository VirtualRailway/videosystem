//-----------------------------------------------------------------------------
//
//		Структуры данных для работы с ообщениями об ошибках
//		(с) РГУПС, ВЖД 17/09/2016
//		Разработал: Притыкин Д.Е.
//
//-----------------------------------------------------------------------------
#ifndef		MSG_H
#define		MSG_H

#include	<QString>

struct TMsg
{
	QString	description;	// Текстовое описание сообщения
	unsigned char type;		// Тип сообщения
	unsigned int code;		// Код сообщения
};

// Типы сообщений
#define		MSG_INFO	0	// Информация
#define		MSG_WARN	1	// Пердупреждение
#define		MSG_ERROR	2	// Ошибка

// Коды информационных сообщений

// Коды предупреждений
#define		WARN_CFG_FILE_NOT_FOUND			1	// Не найден файл конфигурации
#define		WARN_NOT_DOUBLE_DATA			2	// Прочитанные данные не вещественное число
#define		WARN_NOT_INTEGER_DATA			3	// Прочитанные данные не целое число
#define		WARN_FIELD_NOT_FOUND			4	// Не найдено искомое поле в конфигурационном файле
#define		WARN_XML_READER_ERROR			5	// Ошибка чтения XML-файла
#define		WARN_XML_INVALID_ROOT			6	// Неверное имя корневого элемента
#define		WARN_XML_SECTION_NOT_FOUND		7	// Не найдена секция в конфиге
#define		WARN_XML_FIELD_NOT_FOUND		8	// Не найдено поле в секции

// Коды ошибок
#define		ERROR_SERIAL_PORT_NOT_OPENED	1	// Неудачная попытка открытия COM-порта
#define     ERROR_TCP_SERVER_NOT_INIT       2   // Не инициализирован TCP-сервер
#define     ERROR_TCP_SERVER_NOT_STARTED    3   // Не запущен TCP-сервер

#endif

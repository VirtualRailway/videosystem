//------------------------------------------------------------------------------
//
//      Функции для чтения конфигурации видеосистемы
//      (с) РГУПС, 09/01/2017
//      Разработал: Притыкин Д.Е.
//
//------------------------------------------------------------------------------
#ifndef CONFIG_H
#define CONFIG_H

#include    "CfgReader.h"
#include    "common-types.h"
#include    "msg.h"

#define     DEFAULT_WIDTH   1024
#define     DEFAULT_HEIGHT  768
#define     DEFAULT_TIMEOUT 1000

// Чтение конфигурации главного окна
void getWindowConfig(CfgReader cfg, window_config_t &window_config);

void getTcpClientConfig(CfgReader cfg, tcp_config_t &tcp_config);

#endif // CONFIG_H

TEMPLATE = app
TARGET = Video3D

DEPENDPATH += ../QtOgreWindow
INCLUDEPATH += ../QtOgreWindow/include

QT += core
QT += gui
QT += widgets
QT += xml
QT += network

CONFIG += qtestlib

unix {

    INCLUDEPATH += /usr/include/OGRE
    INCLUDEPATH += /usr/include/OGRE/Overlay

    CONFIG += link_pkgconfig
    PKGCONFIG += OGRE
}

win32 {

}

CONFIG(debug, debug | release) {

    TARGET = $$join(TARGET,,,_d)
    LIBS += -lOgreMain -lOIS -lOgreOverlay -lboost_system
    LIBS += -L../lib -lQtOgreWindow_d
    DESTDIR = ../bin

} else {

    LIBS += -lOgreMain -lOIS -lOgreOverlay -lboost_system
    LIBS += -L../lib -lQtOgreWindow
    DESTDIR = ../bin
}

INCLUDEPATH += include/

HEADERS += $$files(include/*.h)

SOURCES += $$files(src/*.cpp)
